package com.example.logindemo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import com.bumptech.glide.Glide;


public class SecondActivity extends AppCompatActivity {

    private EditText editText1,editText2,editText3;
    private ImageView imageView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        editText1=(EditText) findViewById(R.id.editTextTextPersonName);
        editText2=(EditText) findViewById(R.id.editTextTextPersonName2);
        editText3=(EditText) findViewById(R.id.editTextTextPersonName3);
        imageView=(ImageView) findViewById(R.id.imageView);

        String getEmail=getIntent().getExtras().getString("email");
        String getFname=getIntent().getExtras().getString("fname");
        String getLname=getIntent().getExtras().getString("lname");
        String getImage=getIntent().getExtras().getString("image");
        editText1.setText(getEmail);
        editText2.setText(getFname);
        editText3.setText(getLname);
        Log.w("gfgfgf", "onCreate: "+ getImage );
        Glide.with(SecondActivity.this).load(getImage).into(imageView);


    }
}