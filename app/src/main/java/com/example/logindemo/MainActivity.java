package com.example.logindemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.logindemo.adapter.ImageAdapter;
import com.example.logindemo.utils.CommonDB;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String URL_DATA="https://reqres.in/api/users?page=1";
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListView> listView;
    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView1=(TextView)findViewById(R.id.textView4);

        CommonDB commonDB= new CommonDB(getApplicationContext());
        commonDB.getString("Email");
        commonDB.getString("Password");
        textView1.setText("You are login As- "+commonDB.getString("Email"));
        recyclerView= (RecyclerView)findViewById(R.id.recylerview3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        listView= new ArrayList<>();

        loadRecyclerViewData();
        Button logout=findViewById(R.id.logoutButton);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
            }
        });

    }
    private void loadRecyclerViewData(){
        ProgressDialog progressDialog= new ProgressDialog(this);
        progressDialog.setMessage("Loading..!");
        progressDialog.show();
        StringRequest stringRequest= new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject= new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("data");
                    for (int i=0; i<jsonArray.length();i++)
                    {
                        JSONObject o=jsonArray.getJSONObject(i);
                        ListView item= new ListView();
                        item.setEmail(o.getString("email"));
                        item.setFirstName(o.getString("first_name"));
                        item.setLastName(o.getString("last_name"));
                        item.setImageUrl(o.getString("avatar"));
                        listView.add(item);
                    }
                    adapter = new ImageAdapter(getApplicationContext(),listView);
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();

            }
        });
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}

