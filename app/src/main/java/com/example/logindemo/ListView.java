 package com.example.logindemo;

 import java.util.PrimitiveIterator;

 public class ListView {
    private String email;
    private String FirstName;
    private String LastName;
    private String ImageUrl;



     public ListView(){}

     public ListView(String email, String firstName, String lastName, String ImageUrl) {
        this.email = email;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.ImageUrl=ImageUrl;

    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setEmail(String email) {
         this.email = email;
     }

     public void setFirstName(String firstName) {
         FirstName = firstName;
     }

     public void setLastName(String lastName) {
         LastName = lastName;
     }

     public void setImageUrl(String imageUrl) {
         ImageUrl = imageUrl;
     }

    


}
