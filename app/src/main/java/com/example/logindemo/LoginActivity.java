package com.example.logindemo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.logindemo.utils.CommonDB;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.squareup.picasso.Picasso;

public class LoginActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN =101 ;
    private static final String TAG =null ;
    private CallbackManager callbackManager;
    private FirebaseAuth firebaseAuth,mAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private TextView textfacebook;
    private ImageView facebookPhoto;
    private AccessTokenTracker accessTokenTracker;
    public String fbName;
    private SignInButton signInButton;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
        FirebaseUser user =mAuth.getCurrentUser();
        if (user!= null){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    // THIS_IS FOR _FACEBOOK LOGIN
        firebaseAuth=FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
        textfacebook=findViewById(R.id.textFacebook);
        facebookPhoto=findViewById(R.id.imageFacebook);
        facebookPhoto.setVisibility(View.GONE);
        LoginButton facebookLogin=findViewById(R.id.facebook_login);
        facebookLogin.setReadPermissions("email","public_profile");
        callbackManager=CallbackManager.Factory.create();
        facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        authStateListener= new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth1) {
                FirebaseUser user= firebaseAuth1.getCurrentUser();

                if (user!= null){
                    updateUI(user);
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                   fbName= user.getDisplayName();
                    CommonDB data=new CommonDB(getApplicationContext());
                    data.setString("Email",fbName.toString());
                }
                else{
                    updateUI(null);
                }

            }
        };
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken==null){
                    firebaseAuth.signOut();
                }
            }
        };
    //FOR_GOOGLE LOGIN
        mAuth=FirebaseAuth.getInstance();
        googleLoginRequest();
        signInButton=findViewById(R.id.googleButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });





    //THIS_ IS_ FOR_MANUAL_LOGIN_
        EditText editTextEmail = (EditText) findViewById(R.id.editTextTextEmailAddress);
        EditText editTextPassword = (EditText) findViewById(R.id.editTextTextPassword);
        Button Loginbutton = (Button) findViewById(R.id.button);

        ImageButton ib2 = (ImageButton) findViewById(R.id.imageButton5);
        Loginbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (editTextEmail.getText().toString().isEmpty())
                    {
                        editTextEmail.setError("Please enter Email first");
                        Toast.makeText(getApplicationContext(),"Please enter Email Before Proceeding further!!",Toast.LENGTH_SHORT).show();
                    }
                    else if (editTextPassword.getText().toString().isEmpty())
                    {
                        editTextPassword.setError("Please enter Password also");
                        Toast.makeText(getApplicationContext(),"Please enter Password before Proceeding further !!",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        CommonDB data=new CommonDB(getApplicationContext());
                        data.setString("Email",editTextEmail.getText().toString());
                        data.setString("Password",editTextPassword.getText().toString());
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));

                    }



                }
            });

        }

    private void googleLoginRequest() {
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleFacebookToken(AccessToken accessToken) {
        AuthCredential credential= FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    updateUI(user);
                }else {
                        Toast.makeText(getApplicationContext(),"Authentication Failed",Toast.LENGTH_SHORT).show();
                        updateUI(null);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
               
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);

                Toast.makeText(this, "SignIn Failed", Toast.LENGTH_SHORT).show();
                // ...
            }
        }


    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = mAuth.getCurrentUser();
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));

                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(LoginActivity.this, "Sorry auth failed.", Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }

    private void updateUI(FirebaseUser user) {
        if(user!= null){
            textfacebook.setText(user.getDisplayName());
            if(user.getPhotoUrl()!= null){
                facebookPhoto.setVisibility(View.VISIBLE);
                String photoUrl=user.getPhotoUrl().toString();
                photoUrl=photoUrl+"?type=large";
                Glide.with(this).load(photoUrl).into(facebookPhoto);
            }
        }
        else {
            textfacebook.setText("");
            facebookPhoto.setVisibility(View.GONE);
            facebookPhoto.setImageResource(R.drawable.ic_facebook);
        }
    }







    @Override
    protected void onStop() {
        super.onStop();
        if (authStateListener!=null){
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

}


