package com.example.logindemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.logindemo.ListView;
import com.example.logindemo.R;
import com.example.logindemo.SecondActivity;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.Holder>{

        private Context context;
        private List<ListView> listViews;


        public ImageAdapter(Context context, List<ListView> listViews) {
                this.context = context;
                this.listViews = listViews;

        }

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View v;
                LayoutInflater inflater=LayoutInflater.from(context);
                v=inflater.inflate(R.layout.listview,parent,false);
                Holder newholder= new Holder(v);
                newholder.linearLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                              Intent i=new Intent(context,SecondActivity.class);
                              i.putExtra("email",listViews.get(newholder.getAdapterPosition()).getEmail());
                              i.putExtra("fname",listViews.get(newholder.getAdapterPosition()).getFirstName());
                              i.putExtra("lname",listViews.get(newholder.getAdapterPosition()).getLastName());
                              i.putExtra("image",listViews.get(newholder.getAdapterPosition()).getImageUrl());

                              i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(i);
                        }
                });
                return newholder;

        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {

                holder.textviewemail.setText(listViews.get(position).getEmail());
                holder.textviewfirstname.setText(listViews.get(position).getFirstName());
                holder.textviewlastname.setText(listViews.get(position).getLastName());
                Glide.with(context).load(listViews.get(position).getImageUrl()).into(holder.imageView);




        }

        @Override
        public int getItemCount() {

                return listViews.size();
        }

        public class Holder extends RecyclerView.ViewHolder{
                public TextView textviewemail;
                public TextView textviewfirstname;
                public TextView textviewlastname;
                public ImageView imageView;
                public LinearLayout linearLayout;


                public Holder(@NonNull View itemView) {
                        super(itemView);
                        textviewemail=(TextView) itemView.findViewById(R.id.textviewemail);
                        textviewfirstname=(TextView) itemView.findViewById(R.id.textviewfirstname);
                        textviewlastname=(TextView) itemView.findViewById(R.id.textviewlastname);
                        imageView=(ImageView) itemView.findViewById(R.id.imageview1);
                        linearLayout=(LinearLayout)itemView.findViewById(R.id.clicklayout);
                }
        }
}
