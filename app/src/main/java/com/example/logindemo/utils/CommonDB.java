package com.example.logindemo.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class CommonDB {
    Context context;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public CommonDB(Context context) {
        this.context = context;
        pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();



    }
   public void setString(String key, String value)
    {
        editor.putString(key,value);
        editor.commit();
    }
    public String getString(String Key)
    {
        return pref.getString(Key,"");


    }



}
